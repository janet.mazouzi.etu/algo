
#include<stdio.h>
#include<stdlib.h>

struct cell{
	int e;
	struct cell *suiv;
};

void afficherListe(struct cell *liste){
	struct cell* temp = liste;
	if(liste == NULL)
		printf("Liste nulle\n");
	while(temp != NULL){
		printf("%d\n\n\n",temp->e);
		temp=temp->suiv;
	}
}


void ajout_tete(int val, struct cell **pListe){
	struct cell *e1;
	e1 = malloc(sizeof(struct cell));
	e1->e = val;
	//afficherListe(e1);
	e1->suiv = *pListe;	
	*pListe = e1;
}


struct cell * duplicate(struct cell *liste){
	if (liste == NULL)
		return NULL;
	struct cell *new;
	new = malloc(sizeof(struct cell));
	new = NULL;
	
	ajout_tete(liste->e,&new);

	if (liste->suiv == NULL)
		return new;

	struct cell *temp = liste->suiv;
	struct cell ** parcours = &new;

	while (temp != NULL){
		ajout_tete(temp->e,&((*parcours)->suiv));
		parcours = &((*parcours)->suiv);
		temp = temp->suiv;
	}
	return new;
}



void desalouer(struct cell** pliste){
	struct cell* supp = *pliste;
	while (*pliste != NULL){
		*pliste = (*pliste)->suiv;
	       	free(supp);
		supp = *pliste;
	}
       			       
		
}

int main(){
	struct cell *liste;
	struct cell *new;
	liste = NULL;
	new = NULL;
	
	printf("Si liste nulle\n");
	new = duplicate(liste);
	afficherListe(new);
	ajout_tete(3,&liste);

	/*printf("\nSi liste contient 1 cellule\n");
	new = duplicate(liste);
	afficherListe(new);
	desalouer(&new);
	*/

	printf("\nSinon \n");
	ajout_tete(4,&liste);
	ajout_tete(5,&liste);
	new = duplicate(liste);
	afficherListe(new);


	desalouer(&liste);
	desalouer(&new);
	
	return 0;
}
