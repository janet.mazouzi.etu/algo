#include<stdio.h>
#include<stdlib.h>

struct cell{
	int e;
	struct cell *suiv;
};

void afficherListe(struct cell *liste){
	struct cell* temp = liste;
	if(liste == NULL)
		printf("Liste nulle\n");
	while(temp != NULL){
		printf("%d\n\n\n",temp->e);
		temp=temp->suiv;
	}
}


void ajout_tete(int val, struct cell **pListe){
	struct cell *e1;
	e1 = malloc(sizeof(struct cell));
	e1->e = val;
	afficherListe(e1);
	e1->suiv = *pListe;	
	*pListe = e1;
}


void ajout_ord(struct cell **liste, int element){
	if (liste == NULL){
		ajout_tete(element, &liste);
		return;
	}
	if (liste->e > element){
		ajout_tete(element, &liste);
		return;
	}

	struct cell *parcours = *liste;
	while((parcours->suiv != NULL) && (parcours->suiv->e < element)){
		parcours = parcours->suiv;
	}
	ajout_tete(element,parcours->suiv);
}



int main(){
	
	struct cell *cellule;
	cellule = NULL;
	afficherListe(&cellule);
	
	ajout_tete(5,&cellule);
	afficherListe(&cellule);

	ajout_tete(3,&cellule);
	afficherListe(&cellule);

	ajout_tete(2,&cellule);
	afficherListe(&cellule);

	ajout_tete(1,&cellule);
	afficherListe(&cellule);

	ajout_ord(&cellule, 4);
	afficherListe(&cellule);

	ajout_ord(&cellule, 1);
	afficherListe(&cellule);
	return 0;
}
