#include<stdio.h>
#include<string.h>
#define MAX_PERSONNES 10

//définition des structures
struct Date{int jour, mois, annee;} ;

struct Personne{
  
  char nom[20];
  char prenom[20];
  char tel[20];
  struct Date naissance;
};


struct Annuaire{
  
  struct Personne pers[MAX_PERSONNES];
  int dernier;
};

//fonctions d'affichage

void affiche_date(struct Date d){
  printf("j:%d, m: %d, a:%d\n",d.jour,d.mois,d.annee);
}

void affiche_personne(struct Personne p){
  printf("%s\n%s\n%s\n",p.nom,p.prenom,p.tel);
  affiche_date(p.naissance);
}

void affiche_annuaire(struct Annuaire A){
  for(int i=0;i<A.dernier;i++){
    printf("Personne %d:\n",i+1);
    affiche_personne((A.pers[i]));
    printf("\n");
  }
}

//fonctions de lecture

int lire_date(struct Date *d){
  //printf("Veuillez entrer votre date sous la forme jour\nmois\nannée\n");
  scanf("%d\n%d\n%d",&(d->jour),&(d->mois),&(d->annee));
  return 0;
}

int lire_personne(struct Personne * ptr_pers){
  
  if(scanf("%s",ptr_pers->nom)!=EOF){
    
    scanf("%s", ptr_pers->prenom);
    lire_date(&(ptr_pers->naissance));
    scanf("%s",ptr_pers->tel );
    
    return 0;
  }
  return 1;
}

int lire_annuaire(struct Annuaire * A){
  int i=0;
  while ((!(lire_personne(&(A->pers[i])))&&(i<MAX_PERSONNES))){
    //printf("%d\n",(lire_personne(&(A->pers[i]))&&(i<MAX_PERSONNES)));
    
    i++;
  }
  
  A->dernier=i;
  return 0;
}

//fonctions de tri

int compare_dates(struct Date d1, struct Date d2){
  if(d1.annee < d2.annee) return -1;
  else if (d1.annee > d2.annee) return 1;
  else {
    if(d1.mois < d2.mois) return -1;
    else if(d1.mois > d2.mois) return 1;
    else {
      if(d1.jour < d2.jour) return -1;
      else if(d1.jour > d2.jour) return 1;
      else return 0;
    }
  }
}

void permuter_personnes(struct Personne *P1,struct Personne *P2){
  
  struct Personne temp=*P1;
  *P1=*P2;
  *P2=temp;
}

void tri_bulle_dates(struct Annuaire *A){
  
  for(int i=0;i<A->dernier-1;i++){
    for(int j=0;j<A->dernier-i-1;j++){
      
      if(compare_dates(A->pers[i].naissance, A->pers[i+1].naissance)==1)
	permuter_personnes(&(A->pers[i]), &(A->pers[i+1]));
    }
  }  
}

void tri_nom(struct Annuaire* A){
  
  for(int i=0;i<A->dernier-1;i++){
    for(int j=0;j<A->dernier-i-1;j++){
      if(strcmp(A->pers[i].nom,A->pers[i+1].nom)>0)
	permuter_personnes(&(A->pers[i]),&(A->pers[i+1]));
    }
  }
}

int main(){
  //tests partie 2.1
  /*struct Date D;
    lire_date(&D);
    affiche_date(D);*/

  //tests partie 2.2
  /*struct Personne p;
    lire_personne(&p);
    affiche_personne(p);*/

  //tests partie 2.3
  struct Annuaire A;
  lire_annuaire(&A);
  affiche_annuaire(A);

  //tests partie 3
  /*tri_bulle_dates(&A);
  printf("\n ANNUAIRE TRIE PAR DATE\n");
  affiche_annuaire(A);*/
  
  tri_nom(&A);
  printf("\n ANNUAIRE TRIE PAR NOM\n");
  affiche_annuaire(A);
  
  return 0;
}
