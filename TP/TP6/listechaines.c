#include "listechaines.h"



void afficher_liste(struct cell* liste){

	if (liste == NULL){
		printf("La liste est nulle\n");
		return;
	}
	struct cell *temp;
	temp = liste;
 
	printf("Voici la liste:\n");
	while (temp !=  NULL){
		printf("%s\n", temp->val);
		temp = temp->suiv;
 	}
}


void ajout_tete(struct cell **pliste, char *c){
	struct cell *new;
	new = malloc(sizeof(struct cell));
	strcpy(new->val,c);
	new->suiv = *pliste;
	*pliste = new;

}


void  supp_tete(struct cell **pliste){
	if (*pliste == NULL)
		return;
	
	struct cell *temp;
	temp = *pliste;
	*pliste = (*pliste)->suiv;
	free(temp);
	return;	
	

}



//ajout d'un mot dans une liste chainee triée
void ajout_alphab(struct cell ** pl, char * mot)
{
	// liste vide ou mot < mot prochain => ajout en tête
    if ( (*pl == NULL) || (strcmp(mot, (*pl)->val) < 0) )
    {
        ajout_tete(pl,mot);
    }
    else
    {
    	// mot > mot prochain => ajouter après dans la liste
        if (strcmp(mot, (*pl)->val) > 0)
        {
            ajout_alphab(&(*pl)->suiv,mot);
        }
        //else => mot déjà dans la liste, ne rien faire?
    }
}


//construit une liste triee a partir d'un fichier
void charge_fichier(FILE * fp, struct cell ** pl)
{
    char mot[MAXSIZE];

	//Nb d'elements à lire dans chaque fscanf
    const int NB_A_LIRE = 1;

	//`man fscanf` pour comprendre les valeurs de retour!!!
    while (fscanf(fp, "%s", mot) == NB_A_LIRE)
    {
        ajout_alphab(pl, mot);
    }

    //On peut tester la bonne ou mauvaise terminaison de la lecture
    if(feof(fp))    printf("Fin normal de lecture\n");
    if(ferror(fp))  printf("ERREUR de lecture\n");

}


bool appartient(struct cell *liste, char *c){
	if (liste == NULL) return 0;
	if (!strcmp(liste->val,c)) return 1;
	return (appartient(liste->suiv,c));

}

int taille(struct cell *liste){
	if (liste == NULL) return 0;
	
	return taille(liste->suiv)+1;

}




