#include<stdio.h>
#include<string.h>
#define MAX_PERSONNES 10

//définition des structures
struct Date {

    int jour;
    int mois;
    int annee;
};

struct Personne{
  
  char nom[20];
  char prenom[20];
  char tel[20];
  struct Date naissance;
};


struct Annuaire{
  
  struct Personne pers[MAX_PERSONNES];
  int dernier;
};

//fonctions d'affichage

void affiche_date(struct Date d){
  printf("j:%d, m: %d, a:%d\n",d.jour,d.mois,d.annee);
}

void affiche_personne(struct Personne p){
  printf("%s\n%s\n%s\n",p.nom,p.prenom,p.tel);
  affiche_date(p.naissance);
}

void affiche_annuaire(struct Annuaire A){
  for (int i = 0;i<A.dernier+1;i++){
    printf("Personne %d:\n",i+1);
    affiche_personne((A.pers[i]));
    printf("\n");
  }
}

//fonctions de lecture

int lire_date(struct Date *d, FILE *f ){
  //printf("Veuillez entrer votre date sous la forme jour\nmois\nannée\n");
  fscanf(f,"%d\n%d\n%d",&(d->jour),&(d->mois),&(d->annee));
  return 0;
}

int lire_personne(struct Personne * ptr_pers, FILE * f){
  
  if (fscanf(f,"%s",ptr_pers->nom)!= EOF){
    
    fscanf(f,"%s", ptr_pers->prenom);
    lire_date(&(ptr_pers->naissance),f);
    fscanf(f,"%s",ptr_pers->tel );
    
    return 0;
  }
  return 1;
}


int lire_annuaireTP2(struct Annuaire *A, FILE *f){
  int i = 0;
  while ((!(lire_personne(&(A->pers[i]),f))&&(i<MAX_PERSONNES))){
    i++;
  }
  A->dernier=i-1;
  return 0;
}

//fonctions de tri

int compare_dates(struct Date d1, struct Date d2){
  if (d1.annee < d2.annee) 
      return -1;

  else if (d1.annee > d2.annee) 
      return 1;

  else {
    if (d1.mois < d2.mois) 
        return -1;
    else if (d1.mois > d2.mois) 
        return 1;
    else {
      if(d1.jour < d2.jour) 
          return -1;
      else if(d1.jour > d2.jour) 
          return 1;
      else 
          return 0;
    }
  }
}

void permuter_personnes(struct Personne *P1,struct Personne *P2){
  
  struct Personne temp = *P1;
  *P1 = *P2;
  *P2 = temp;
}

void tri_bulle_dates(struct Annuaire *A){
  
  for (int i = 0;i<A->dernier;i++){
    for (int j = 0;j<A->dernier-i;j++){
      
      if (compare_dates(A->pers[i].naissance, A->pers[i+1].naissance) == 1)
	permuter_personnes(&(A->pers[i]), &(A->pers[i+1]));
    }
  }  
}

void tri_nom(struct Annuaire* A){
  
  for (int i = 0;i<A->dernier;i++){
    for (int j = 0;j<A->dernier-i;j++){
      if (strcmp(A->pers[i].nom,A->pers[i+1].nom)>0)
	permuter_personnes(&(A->pers[i]),&(A->pers[i+1]));
    }
  }
}

//fonctions de recherche et de modification

int rech_dicho(struct Annuaire * A, char *p){
  int borne_sup = A->dernier+1;
  int borne_inf = 0;
  int mid;
  int pos = -1;
  
  while ((borne_sup-borne_inf>0)&&(pos == -1)){
    mid = (borne_sup+borne_inf)/2;
    
    
    if (strcmp(A->pers[mid].nom,p)>0)
      borne_sup = mid-1;
    
    else if (strcmp(A->pers[mid].nom,p)<0)
      borne_inf = mid+1;
    
    else pos = mid;
  }
  
  if (strcmp(A->pers[borne_sup].nom,p) == 0){
    return borne_sup;
  }
  else 
      return pos;
  
}

void modif_num(struct Annuaire *A,char *p){
  int i = rech_dicho(A,p);
  
  if (i == -1){
    printf("Personne inconnue à l'annuaire\n");
    return;
  }
  
  else{
    printf("Le numéro enregistré pour cette personne est le %s . Veuillez rentrer le nouveau numéro: ",A->pers[i].tel);
    scanf("%s",A->pers[i].tel);
    printf("\n numéro enregistré avec succés\n");
    
  }
}


//fonction de sauvergarde
void new_file(struct Annuaire A,FILE *new){
  

  for (int i = 0;i<A.dernier+1;i++){
    
    fprintf(new,"%s\n%s\n",A.pers[i].nom,A.pers[i].prenom);
    fprintf(new,"%d\n%d\n%d\n",A.pers[i].naissance.jour,A.pers[i].naissance.mois,A.pers[i].naissance.annee);
    fprintf(new,"%s\n", A.pers[i].tel);
  }

}



//MENU

int menu(void){
  int choix;
  printf("\nVeuillez rentrer le chiffre correspondant à la fonctionnalité dont vous avez besoin:\n");
  printf(" 1 : Afficher votre annuaire\n");
  printf(" 2 : Rechercher une personne dans l'annuaire\n");
  printf(" 3 : Modification d'un numéro de téléphone\n");
  printf(" 4 : Quitter l'application avec sauvegarde\n");
  printf(" 5 : Quitter l'application sans sauvegarde\n\n");

  scanf("%d",&choix);
  return choix;
}
  

int main(){

  //ouverture du fichier avec l'annuaire
  FILE *fd;
  fd = fopen("annu.txt", "r");

  //création et tri de l'annuaire
  struct Annuaire A;
  lire_annuaireTP2(&A,fd);
  tri_nom(&A);

  //fermeture du fichier
  fclose(fd);
 
  
  //test arguments main. Ajouter les arguments du main avant de compiler.
  /*
    int i = 0;
    printf("Bonjour, il y a %d argument(s) à cette commande! \n",argc);

    while (i<argc)
    {
    printf("argument %d : %s\n",i,argv[i]);
    i++;
    }
  
    printf("c'est fini !\n");
  */

  

  //MENU

  printf("\n\n***\n\nBIENVENUE\n\n***\n\n");
  printf("NB: l'annuaire est automatiquement trié par nom, donc en cas de sauvergarde, l'ordre pourrait être modifié.\n\n");
  int choix = menu();
  char p[20];

  
  while ((choix!= 4)&&(choix!= 5)){

    if ((choix<1)&&(choix>3)) //erreur de saisie
      printf("Saisie non valide, veuillez réessayer\n");
    
    else if (choix == 1) //affichage de l'annuaire
      affiche_annuaire(A);
    
    else if (choix == 2){ //recherche et affichage d'une personne de l'annuaire
      printf("Veuillez rentrer le nom de la personne à rechercher dans l'annuaire : ");
      scanf("%s",p);
      int res = rech_dicho(&A,p);

      if(res == -1) 
          printf("inconnu\n");
      else 
          affiche_personne(A.pers[res]);
    }

    else{ //modification d'un numéro
      printf("Veuillez rentrer le nom de la personne dont vous voulez changer le numéro : ");
      scanf("%s",p);
      modif_num(&A,p);

    }

    choix = menu();

  }

  if(choix == 4){ //sauvegarde du nouvel annuaire
    //création d'un nouveau fichier texte avec l'annuaire modifié

    FILE *new;
    new = fopen("new.txt","w");
    new_file(A,new);
    fclose(new);

  }
  
  return 0;
}
