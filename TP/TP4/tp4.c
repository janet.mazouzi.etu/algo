#include<stdio.h>
#include<stdlib.h>

struct couple{
	int e;
	unsigned int multi;
};


struct cell{
	struct couple c;
	struct cell* suivant;
};

	

void ajout_tete( struct cell** pl, int elem, unsigned int quantite){
	struct cell * n_ptr;
 	struct couple n_couple;	
	n_ptr = malloc(sizeof(struct cell));
	n_couple.e = elem;
	n_couple.multi = quantite;
        n_ptr->c = n_couple;	
	n_ptr->suivant = *pl;
	*pl = n_ptr;
}

struct cell* creation(){
	struct cell *new;
	new = malloc(sizeof(struct cell));
	new = NULL;
	int elem;
	unsigned int mult;
	int compt=0;
	int continuer = 1;
	while (continuer){
		printf("Veuillez rentrer les valeurs avec leurs"); 
		printf(" multiplicités sous la forme e,m\n");
		scanf("%d,%d",&elem,&mult);
		ajout_tete(&new,elem,mult);
		compt++;
		printf("Ajout réussi, continuer?\n   1: oui\n   0: non\n");
		scanf("%d",&continuer);
		while ((continuer != 0) && (continuer != 1)){
			printf("Saisi invalide, veuillez réesayer.\n");
			scanf("%d",&continuer);
		}
	
	}
	//printf("%d\n",compt);
	return new;

}



void print_liste(struct cell *liste){
	if (liste == NULL){
		printf("La liste est nulle\n");
		return;
	}
	struct cell *temp;
	temp = liste;

	printf("Voici la liste:\n");
	while (temp !=  NULL){
		printf("(%d,%d)\n", temp->c.e,temp->c.multi);
		temp = temp->suivant;
	}
	return;
	
}



int suppr_tete(struct cell **pliste){
	if (*pliste == NULL)
		return 0;
	
	struct cell *temp;
	temp = *pliste;
	*pliste = (*pliste)->suivant;
	free(temp);
	return 1;	
	

}


int insertion_recu(struct cell **ppens, int x){
	if ((*ppens == NULL)||((*ppens)->c.e>x)){
		ajout_tete(ppens,x,1);
		return 1;
	}
	struct cell *temp;
	temp=*ppens;
	while ((temp != NULL)&&((temp)->c.e < x))
		return insertion_recu(&(temp->suivant),x);

	if (temp->c.e == x){
		temp->c.multi++;
		return 1;
	
	}
				
	return 0;
	
}

void creationfile(struct cell **ppens, FILE *f){
	int i = 0; 
	while (fscanf(f,"%d",&i) != EOF) {
		insertion_recu(ppens,i);
	}
}

int enstrie(struct cell *pens){
	if ((pens == NULL) || (pens->suivant == NULL)) return 1;
	if(pens->c.e > pens->suivant->c.e) return 0;


	return enstrie(pens->suivant);
}
		




void detruireens(struct cell **ppens){
	if( *ppens == NULL)
		return;

	struct cell **temp = &((*ppens)->suivant);
	ppens = &((*ppens)->suivant);
	free(*ppens);
	return detruireens(temp);
	
	

}



int main(){
	//Tests TP3
	struct cell* liste;
	liste = NULL;
	//liste = creation();
	/*ajout_tete(&liste,4,5);
	ajout_tete(&liste,3,4);
	ajout_tete(&liste,2,5);
	ajout_tete(&liste,1,2);
	
	
	print_liste(liste);
		
	suppr_tete(&liste);
	printf("liste après suppression tete\n");
	print_liste(liste);
	*/

	//Tests TP4
	
	/*	
	insertion_recu(&liste,4);
	printf("liste après insertion\n");
	print_liste(liste);
	*/
	
	FILE *fd;
	fd = fopen("entiers.txt", "r");	
	
	creationfile(&liste,fd);
	fclose(fd);
	print_liste(liste);
	//printf("%d\n",enstrie(liste));
	
//	detruireens(&liste);
	
	free(liste);
	printf("%ld\n",sizeof(struct cell));
	return 0;

}

