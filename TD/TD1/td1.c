#include<stdio.h>
#define NN 5
#define NBR 4

typedef struct Date {int jour, mois, annee;} Date;

typedef struct Eleve{
  char nom[30], prenom[30];
  Date date;
  float notes[NN];
} Eleve;

typedef Classe {Eleve eleve[NBR];} Classe;

int compare(Date d1, Date d2){
  if(d1.annee < d2.annee) return -1;
  else if (d1.annee > d2.annee) return 1;
  else {
    if(d1.mois < d2.mois) return -1;
    else if(d1.mois > d2.mois) return 1;
    else {
      if(d1.jour < d2.jour) return -1;
      else if(d1.jour > d2.jour) return 1;
      else return 0;
    }
  }
}

float moyenne(Eleve eleve){
  int somme=0;
  for(int i=0;i<NN;i++) somme=somme+eleve.notes[i];
  return somme*1.0/NN;
}

int main(){
  /*printf("%d\n",sizeof(Date));*/
  Date d1 = {01,01,2000};
  Date d2 = {01,01,2000};
  printf("%d\n",compare(d1,d2));
  Eleve E = {"Dumont","Margot",d1,{10,12,20,13,14}};
  printf("%.2f\n",moyenne(E));
  
  return 0;
  
}
